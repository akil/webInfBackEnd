package com.composer.webinf.service;

public interface SecurityService {

  public Boolean hasProtectedAccess();

}
