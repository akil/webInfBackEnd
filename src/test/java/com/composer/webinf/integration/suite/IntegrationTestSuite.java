package com.composer.webinf.integration.suite;

import com.composer.webinf.integration.controller.rest.AuthenticationControllerTest;
import com.composer.webinf.integration.controller.rest.ProtectedControllerTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
  AuthenticationControllerTest.class,
  ProtectedControllerTest.class
})
public class IntegrationTestSuite {

}
